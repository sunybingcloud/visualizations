This directory contains 2 main scripts:
1. execution_wait_time_plots_split.py
   Script can be used to plot non_npi_let_benchmarks data and npi_let_benchmarks data separately. Will need slight modification of code to plot it all together. Use benchmark_tick_names to plot all benchmarks together. Mix and match from (npi_let/npi_het/pi_let/npi_het)_benchmark_tick_names to have required benchmark graphs in the plot.
   Usage: python3 execution_wait_time_plots_split.py <experiments.json>
   where <experiments.json> contains the mapping between each experiment and its corresponding runtime metrics data file. Example json  file is experiments_runtimeMetrics.json.
   where runtime metrics file line format is as follows: 
   Date Time TaskID,SubmitTime(yyyy/mm/dd hh:mm:ss),WaitTime(seconds),ExecusionStartTime(yyyy/mm/dd hh:mm:ss),ExecutionFinishTime(yyyy/mm/dd hh:mm:ss),ExecutionTime(seconds)

2. average_task_category_distribution.py
   Script can be used to plot multi stacked bar graph for experiments where each experiment has bars in a group of 3 representing the 3 power classes. Each bar is stacked having 4 components: 1 for each category of benchmark.
   Usage: python3 average_task_category_distribution.py <experiments.json>
   where <experiments.json> contains the mapping between each experiment and its corresponding allocation metrics data file. Example json file is experiments_allocation_metrics.json.
   where allocation metrics file line format is as follows:
   Date Time <host1>:<taskID1>:CPU_SHARE,ACTUAL_SHARE_CPU(%),ACTUAL_SHARE_POWER(Watts),MAX_SHARE_POWER(Watts)-<taskID2>:CPU_SHARE,ACTUAL_SHARE_CPU(%),ACTUAL_SHARE_POWER(Watts),MAX_SHARE_POWER(Watts)|<host2>...
