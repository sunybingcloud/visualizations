'''
Script can be used to plot multi stacked bar graph for experiments where each experiment has bars in a group of 3 representing the 3 power classes. Each bar is stacked having 4 components: 1 for each category of benchmark.
'''

import sys
import json
import plotly.graph_objects as go
import plotly.io as pio
from plotly.offline import plot

# machine to power class mapping, hard coded, needs to be generalized
hostPowerClass = {"stratos-001.cs.binghamton.edu": "classC", "stratos-002.cs.binghamton.edu": "classC"
, "stratos-003.cs.binghamton.edu": "classC", "stratos-004.cs.binghamton.edu": "classC"
, "stratos-005.cs.binghamton.edu": "classA", "stratos-006.cs.binghamton.edu": "classA"
, "stratos-007.cs.binghamton.edu": "classB", "stratos-008.cs.binghamton.edu": "classB"}

#benchmark to category mapping, again, hard coded.
benchmarkCategory = {
    "minife": "NPI-LeT", "stream": "NPI-LeT", "jython": "NPI-LeT", "batik": "NPI-LeT", "avrora": "NPI-LeT"
    , "fop": "NPI-LeT", "luindex": "NPI-LeT", "pmd": "NPI-LeT", "network-loopback": "NPI-LeT"
    , "lusearch": "PI-LeT", "sunflow": "PI-LeT", "tomcat": "PI-LeT", "h2": "NPI-HeT", "eclipse": "NPI-HeT"
    , "tradebeans": "NPI-HeT", "audio-encoding": "NPI-HeT", "dgemm": "PI-HeT", "xalan": "PI-HeT"
    , "video-encoding": "PI-HeT", "cryptography": "PI-HeT" 
}

# all different experiments (scheduling algorithms)
experiments = ["FF", "BP", "0pWTol", "25pWTol", "50pWTol", "75pWTol", "100pWTol", "Wls-WTol", "Wls-WTol-PI-NPI"]
# power classes
powerClasses = ["classA", "classB", "classC"]
# categories of benchmarks in terms of power consumption and execution times. In each tuple, the second element signifies on which stack in the bar, this stack sits on.
categories = [("NPI-LeT",""), ("NPI-HeT","NPI-LeT"), ("PI-LeT","NPI-HeT"), ("PI-HeT","PI-LeT")]

'''
getPowerClassCategoryAverageTasksData function can be used to get a mapping for each experiment, how many tasks ran on an average for each power class for the given workload.
Parameters:
fileName -> json file storing the log file paths for each experiment

Return:
map[experiment][powerClass][benchmark_category]AverageNumberOfTasksOfThatCategory
'''
def getPowerClassCategoryAverageTasksData(fileName):
    # from the json file
    with open(fileName) as fp:
        experiments_dict = dict()
        experiments = json.load(fp)
        # read log file for each experiment
        for experiment, value in experiments.items():
            value_file = open(value)
            lines = value_file.readlines()
            value_file.close()
            # for each line in the log file
            # line format: Date Time <host1>:<taskID1>:CPU_SHARE,ACTUAL_SHARE_CPU(%),ACTUAL_SHARE_POWER(Watts),MAX_SHARE_POWER(Watts)-<taskID2>:CPU_SHARE,ACTUAL_SHARE_CPU(%),ACTUAL_SHARE_POWER(Watts),MAX_SHARE_POWER(Watts)|<host2>...
            for line in lines[1:]:
                hostTaskData = (line.split(" ")[2]).split("|")
                # get host:task:data for each host
                for hostTasks in hostTaskData:
                    host = hostTasks.split(":")[0]
                    categoryCountUpdated = {"NPI-LeT": False, "NPI-HeT": False, "PI-LeT": False, "PI-HeT": False}
                    # for each benchmark
                    for benchmark in benchmarkCategory.keys():
                        # count number of instances for each benchmark at that point of time of the log in the particular host
                        count = hostTasks.count(benchmark)
                        if(count > 0):
                            if(experiment in experiments_dict):
                                if(host in experiments_dict[experiment]):
                                    if(benchmarkCategory[benchmark] in experiments_dict[experiment][host]):
                                        # increment the task count for that particular benchmark category by count
                                        experiments_dict[experiment][host][benchmarkCategory[benchmark]][0] += count
                                        # increment the number of seconds a benchmark of a particular category runs by 1 if not already account for this line
                                        experiments_dict[experiment][host][benchmarkCategory[benchmark]][1] += 1 if not categoryCountUpdated[benchmarkCategory[benchmark]] else 0
                                    else:
                                         experiments_dict[experiment][host][benchmarkCategory[benchmark]] = [count, 1]
                                else:
                                    experiments_dict[experiment][host] = {benchmarkCategory[benchmark]: [count, 1]}
                            else:
                                experiments_dict[experiment] = {host: {benchmarkCategory[benchmark]: [count, 1]}}
                            categoryCountUpdated[benchmarkCategory[benchmark]] = True

        # find the average of number of tasks in a category(count/seconds)
        for experiment in experiments_dict:
            for host in experiments_dict[experiment]:
                for category in experiments_dict[experiment][host]:
                    experiments_dict[experiment][host][category].append(experiments_dict[experiment][host][category][0]/experiments_dict[experiment][host][category][1])
        
        #find the average number of taks in a power class
        experimentDictAveragePowerClass = dict()
        for experiment in experiments_dict:
            if(experiment not in experimentDictAveragePowerClass):
                experimentDictAveragePowerClass[experiment] = dict()
            for host in experiments_dict[experiment]:
                if(hostPowerClass[host] not in experimentDictAveragePowerClass[experiment]):
                    experimentDictAveragePowerClass[experiment][hostPowerClass[host]] = dict()
                    for category in experiments_dict[experiment][host]:
                        if(category in experimentDictAveragePowerClass[experiment][hostPowerClass[host]]):
                            experimentDictAveragePowerClass[experiment][hostPowerClass[host]][category] += experiments_dict[experiment][host][category][2]
                        else:
                            experimentDictAveragePowerClass[experiment][hostPowerClass[host]][category] = experiments_dict[experiment][host][category][2]
        
        for experiment in experimentDictAveragePowerClass:
            for hostClass in experimentDictAveragePowerClass[experiment]:
                for category in experimentDictAveragePowerClass[experiment][hostClass]:
                    experimentDictAveragePowerClass[experiment][hostClass][category] /= 4 if hostClass=="classC" else 2
        
        return experimentDictAveragePowerClass

'''
plotMultiStackedBarGraph function can be used to plot stacked multi bar graphs. Here the four categories of the benchmarks are stacked in a bar and 1 bar is used for each power class and 3 power classes per experiment.

Parameters:
data -> map[experiment][powerClass][benchMarkCategory]Value
graphTitle -> String: Title of the graph
fileName -> String: file name to save the graph with
'''
def plotMultiStackedBarGraph(data, graphTitle, fileName):
    bar_data = []
    mapped_bar_data = {}
    # get values for category, powerClass in a map
    for (category,base) in categories:
        for powerClass in powerClasses:
            y_axis=[]
            for experiment in experiments:
                y_axis.append(data[experiment][powerClass][category])
            if category in mapped_bar_data:
                mapped_bar_data[category][powerClass] = y_axis
            else:
                mapped_bar_data[category] = {powerClass: y_axis}

    # the key to build a stacked graph is the parameter offsetgroup which signifies which bar in a grouped bar graph this stack belongs to.
    # paramter base signifies where the bottom y point should be placed for this stack bar.
    for (index,powerClass) in enumerate(powerClasses):
        x_axis = experiments
        bar_data.append(go.Bar(name="NPI-LeT", x=x_axis, y=mapped_bar_data["NPI-LeT"][powerClass], offsetgroup=index, base=0, marker_color="rgb(255, 95, 14)", showlegend=False if index != 0 else True))#red
        bar_data.append(go.Bar(name="NPI-HeT", x=x_axis, y=mapped_bar_data["NPI-HeT"][powerClass], offsetgroup=index, base=mapped_bar_data["NPI-LeT"][powerClass], marker_color="rgb (244, 208, 63)", showlegend=False if index != 0 else True))#yellow
        bar_data.append(go.Bar(name="PI-LeT", x=x_axis, y=mapped_bar_data["PI-LeT"][powerClass], offsetgroup=index, base=[x[0]+x[1] for x in zip(mapped_bar_data["NPI-LeT"][powerClass], mapped_bar_data["NPI-HeT"][powerClass])], marker_color="rgb(34, 153, 84)", showlegend=False if index != 0 else True))#green
        bar_data.append(go.Bar(text=powerClass[-1], textposition="outside", name="PI-HeT", x=x_axis, y=mapped_bar_data["PI-HeT"][powerClass], offsetgroup=index, base=[x[0]+x[1]+x[2] for x in zip(mapped_bar_data["NPI-LeT"][powerClass], mapped_bar_data["NPI-HeT"][powerClass], mapped_bar_data["PI-LeT"][powerClass])], marker_color="rgb(31, 119, 180)", showlegend=False if index != 0 else True))#blue
    

    fig = go.Figure(data=bar_data)
    fig.update_traces(marker_line_color='rgb(8,48,107)',marker_line_width=0.5, opacity=1)
    fig.update_layout(xaxis_tickangle=-45, margin=dict(l=5, r=5, t=5, b=5),barmode="group", yaxis_title="Average number of Co-located Tasks", xaxis_title="Experiments")
    fig.update_layout(font=dict(family="arial",size=15,color="#000"))
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.98,
        xanchor="right",
        x=0.99
    ))
    plot(fig, filename=fileName, image='png', image_filename=fileName)    
    #fig.show()

if __name__ == "__main__":
    if(len(sys.argv) != 2):
        print("Usage: python3 average_task_category_distribution.py <json file path containing the experiments data, allocation metrics log file>")
        sys.exit()
    # extract data for task distribution over different power classes
    data = getPowerClassCategoryAverageTasksData(sys.argv[1])
    # plot multi stacked bar graph for each power class for each experiment
    plotMultiStackedBarGraph(data, "Task Distribution", "Task Distribution")
