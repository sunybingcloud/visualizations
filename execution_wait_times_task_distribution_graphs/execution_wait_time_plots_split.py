'''
Script can be used to plot non_npi_let_benchmarks data and npi_let_benchmarks data separately.
Will need slight modification of code to plot it all together.
Use benchmark_tick_names to plot all benchmarks together.
Mix and match from (npi_let/npi_het/pi_let/npi_het)_benchmark_tick_names to have required benchmark graphs in the plot.
'''

import sys
import json
import plotly.graph_objects as go
import plotly.io as pio
from plotly.offline import plot

experiments = ["FF", "BP", "0pWTol", "25pWTol", "50pWTol", "75pWTol", "100pWTol", "Wls-WTol", "Wls-WTol-PI-NPI"]
# benchmarks are in the order of NPI-LeT, PI-LeT, NPI-HeT, PI-HeT
npi_let_benchmarks_tick_names = [("minife","minife"), ("stream","stream"), ("jython","jython"), ("batik","batik"),
 ("avrora","avrora"), ("fop", "fop"), ("luindex","luindex"), ("pmd","pmd"), ("network-loopback","network-loopback")]
pi_let_benchmarks_tick_names = [("lusearch", "lusearch(PI,LeT)"), ("sunflow", "sunflow(PI,LeT)"), ("tomcat", "tomcat(PI,LeT)")]
npi_het_benchmarks_tick_names = [("h2","h2(NPI,HeT)"), ("eclipse","eclipse(NPI,HeT)"), ("tradebeans","tradebeans(NPI,HeT)"), ("audio-encoding", "audio-encoding(NPI,HeT)")]
pi_het_benchmarks_tick_names = [("dgemm","dgemm(PI,HeT)"), ("xalan","xalan(PI,HeT)"), ("video-encoding","video-encoding(PI,HeT)"), ("cryptography","cryptography(PI,HeT)")]
non_npi_let_benchmarks_tick_names = npi_het_benchmarks_tick_names + pi_let_benchmarks_tick_names + pi_het_benchmarks_tick_names
benchmarks_tick_names = npi_let_benchmarks_tick_names + non_npi_let_benchmarks_tick_names

'''
plotMultiBarGraph function can be used to plot graphs for the execution time or wait time for benchmarks run in different experiments(different scheduling algorithms)
Parameters:
data -> data used to plot the graph. A map of type: map[benchmark_name][experiment_name]averageValue<ExecTime/WaitTime>
graphTitle -> String: title for the graph
fileName -> String: fileName for the saving graph in png format
'''
def plotMultiBarGraph(data, graphTitle, fileName):
    non_npi_let_bar_data = []
    npi_let_bar_data = []
    # for each experiment in the list (hard coded based on the experiment list)
    # TODO: remove the hard coding to make it general
    # the key for the stacked multi bar graph is grouping them with the name
    # eg: go.Bar(name=experiment, x=[x[1] for x in non_npi_let_benchmarks_tick_names], y=y_axis)
    # in the example above, x-axis represents one bar per experiment for each benchmark, therefore the above graph even though is for a benchmark, the experiments are tied together based on the experiment name
    for experiment in experiments:
        # non-npi-let benchmarks data
        y_axis = []
        # grouping all values for a benchmark together
        for benchmark in non_npi_let_benchmarks_tick_names:
            y_axis.append(data[benchmark[0]][experiment])
            
        non_npi_let_bar_data.append(go.Bar(name=experiment, x=[x[1] for x in non_npi_let_benchmarks_tick_names], y=y_axis))
        #npi-let benchmarks data
        y_axis = []
        for benchmark in npi_let_benchmarks_tick_names:
            y_axis.append(data[benchmark[0]][experiment])
        
        npi_let_bar_data.append(go.Bar(name=experiment, x=[x[1] for x in npi_let_benchmarks_tick_names], y=y_axis))
    

    non_npi_let_fig = go.Figure(data=non_npi_let_bar_data)
    npi_let_fig = go.Figure(data=npi_let_bar_data)

    non_npi_let_fig.update_traces(marker_line_color='rgb(8,48,107)',marker_line_width=1, opacity=1)
    non_npi_let_fig.update_layout(title_text=graphTitle + " - NPI-HeT, PI-LeT, PI-HeT benchmarks", barmode='group', yaxis_title="seconds", xaxis_title="benchmarks")

    npi_let_fig.update_traces(marker_line_color='rgb(8,48,107)',marker_line_width=1, opacity=1)
    npi_let_fig.update_layout(title_text=graphTitle + " - NPI-LeT benchmarks", barmode='group', yaxis_title="seconds", xaxis_title="benchmarks")
    
    non_npi_let_fig.update_layout(legend=dict(
        orientation="h",
        yanchor="bottom",
        y=1.02,
        xanchor="right",
        x=1
    ))

    npi_let_fig.update_layout(legend=dict(
        orientation="h",
        yanchor="bottom",
        y=1.02,
        xanchor="right",
        x=1
    ))
    plot(non_npi_let_fig, filename=fileName+"-Non-NPI-LeT", image='png', image_filename=fileName+"-Non-NPI-LeT")
    plot(npi_let_fig, filename=fileName+"-NPI-LeT", image='png', image_filename=fileName+"-NPI-LeT")    
    #non_npi_let_fig.show()
    #npi_let_fig.show()

'''
This function can be used to extract average execution time or wait time depending on the dataType parameter.
Parameters:
fileName - The json file having mapping for an experiment to its path to the csv file containing the data
dataType - "exec" or "wait" depending on which of the two is being extracted

Return:
experiments_dict - A dictionary containing a mapping as follows:
map[benchmark_name][experiment_name]averageValue<ExecTime/WaitTime>
'''
def extractAverageExecWaitTimes(fileName, dataType):
    with open(fileName) as fp:
        experiments_dict = dict()
        experiments = json.load(fp)
        # for csv data file for each experiment
        for key, value in experiments.items():
            value_file = open(value)
            lines = value_file.readlines()
            # benchmarks holds all the data related to each benchmark for the current experiment
            benchmarks = dict()
            # for each line recorded in the data file, get the benchmarkName and append the corresponding value to its list
            # line format:
            # Date Time TaskID,SubmitTime(yyyy/mm/dd hh:mm:ss),WaitTime(seconds),ExecusionStartTime(yyyy/mm/dd hh:mm:ss),ExecutionFinishTime(yyyy/mm/dd hh:mm:ss),ExecutionTime(seconds)
            for line in lines[1:]:
                lineParts = line.split(",")
                benchmarkParts = lineParts[0].split("-")
                benchmarkName = benchmarkParts[1]
                for part in benchmarkParts[2:]:
                    try:
                        num = int(part)
                        break
                    except:
                        benchmarkName += "-"+part
                dataTypePosition = -1 if dataType == "exec" else 2
                if(benchmarkName in benchmarks):
                    benchmarks[benchmarkName] += [float(lineParts[dataTypePosition])]
                else:
                    benchmarks[benchmarkName] = [float(lineParts[dataTypePosition])]
            value_file.close()
            # find the average of all the execution times(of each instance run of the experiment)
            for key_b, value_b in benchmarks.items():
                if(key_b in experiments_dict):
                    experiments_dict[key_b][key] = sum(value_b)/len(value_b)
                else:
                    experiments_dict[key_b] = {key: sum(value_b)/len(value_b)}
    
    return experiments_dict

if __name__ == "__main__":
    if(len(sys.argv) != 2):
        print("Usage: python3 execution_wait_time_plots_split.py <experiments.json> " +
        "where experiments.json contains the mapping between each experiment and its corresponding runtime metrics data file")
    # extract execution time data
    exec_data = extractAverageExecWaitTimes(sys.argv[1], "exec")
    # extract wait time data
    wait_data = extractAverageExecWaitTimes(sys.argv[1], "wait")

    # plot execution time data
    plotMultiBarGraph(exec_data, "Exec times", "ExecTimePlot")
    # plot wait time data
    plotMultiBarGraph(wait_data, "Wait times", "WaitTimePlot")
