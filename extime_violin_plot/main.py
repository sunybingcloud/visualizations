import pandas as pd
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import plot
from plotly import tools
import plotly.io as pio
import glob
import os

base_dir = "../../LOGS-electron-SC2019-Watts-as-a-Resource/"
task_name = "lusearch"

task_types = {
	"lusearch": "PILeT",
	"jython": "NPILeT",
	"cryptography": "PIHeT",
	"h2": "NPIHeT"
}

folders = {
	"BP": "Electron-BP-medmedmedpu_2019-March-27_10-32-47",
	"FF": "Electron-FF-medmedmedpu_2019-March-15_19-19-24",
	"U-WTol-0%": "no-alignment/Electron-WaaR-medmedmedpu-cmw-noAlignScore-0perctolerance-cpuShrink50perc_2019-March-15_3-28-43",
	"U-WTol-25%": "no-alignment/Electron-WaaR-medmedmedpu-cmw-noAlignScore-25perctolerance-cpuShrink50perc_2019-March-16_10-9-0",
	"U-WTol-50%": "no-alignment/Electron-WaaR-medmedmedpu-cmw-noAlignScore-50perctolerance-cpuShrink50perc_2019-March-15_11-36-15",
	"U-WTol-75%": "no-alignment/Electron-WaaR-medmedmedpu-cmw-noAlignScore-75perctolerance-cpuShrink50perc_2019-March-14_16-36-42",
	"U-WTol-100%": "no-alignment/Electron-WaaR-medmedmedpu-cmw-noAlignScore-100perctolerance-cpuShrink50perc_2019-March-14_10-36-44",
	"Cr-WTol(0/100C)": "no-alignment-specialtasks-crypto-video/Electron-WaaR-medmedmedpu-cmw-noAlignScore-0pTol-N-2Tasks_100pTol-cryptoVideo-cpuShrink50perc_2019-March-20_19-29-23",
	"Cr-WTol(25/75C)": "no-alignment-specialtasks-crypto-video/Electron-WaaR-medmedmedpu-cmw-noAlignScore-25pTol-N-2Tasks_75pTol-cryptoVideo-cpuShrink50perc_2019-March-20_13-16-34",
	"Cr-WTol(75/25C)": "no-alignment-specialtasks-crypto-video/Electron-WaaR-medmedmedpu-cmw-noAlignScore-75pTol-N-2Tasks_25pTol-cryptoVideo-cpuShrink50perc_2019-March-19_22-14-26",
	"Cr-WTol(100/0C)": "no-alignment-specialtasks-crypto-video/Electron-WaaR-medmedmedpu-cmw-noAlignScore-100pTol-N-2Tasks_0pTol-cryptoVideo-cpuShrink50perc_2019-March-19_16-46-31",
	"WlS-WTol": "custom-tolerance-sectionBasedAllTasks/Electron-WaaR-medmedmedpu-cmw-noAlignScore-customTol-sectionBasedAllTasks-cpuShrink50perc_2019-March-25_20-15-7",
	"Wls-WTol-PI-NPI": "custom-tolerance-sectionBased-PI-NPI-Tasks/Electron-WaaR-medmedmedpu-cmw-noAlignScore-customTol-sectionBased-PI-NPI-Tasks-cpuShrink50perc-Run2_2019-April-1_22-42-57",
	"Wls-WTol-PI-NPI-HeT-LeT": "custom-tolerance-sectionBased-HeT-LeT-PI-NPI-Tasks/Electron-WaaR-medmedmedpu-cmw-noAlignScore-customTol-scaledWTol-favorTAT-HeT-sectionBased-HeT-LeT-PI-NPI-Tasks-cpuShrink50perc_2019-April-1_16-17-24" 
}

run_names = ["FF", "BP", "U-WTol-0%", "U-WTol-25%", "U-WTol-50%", "U-WTol-75%", "U-WTol-100%", "Cr-WTol(0/100C)", "Cr-WTol(25/75C)", "Cr-WTol(75/25C)", "Cr-WTol(100/0C)", "WlS-WTol", "Wls-WTol-PI-NPI", "Wls-WTol-PI-NPI-HeT-LeT"]

data = []

for run_name in run_names:
	run_folder = folders[run_name]
	glob_path = os.path.join(base_dir, run_folder, "*_runtimeMetrics.log")
	matching_filenames = glob.glob(glob_path)
	print(glob_path)
	print(matching_filenames)
	assert(len(matching_filenames)==1)

	filename = matching_filenames[0]

	df = pd.read_csv(filename, sep=',')
	header_names = df.columns.values

	extimes_this_run = df[header_names[-1]][df[header_names[0]].str.contains(task_name)]

	trace = {
		"type": "violin",
		# "x": run_name,
		"y": extimes_this_run,
		"name": run_name,
		"box": {
			"visible": True
		},
		"meanline": {
			"visible": True
		}
	}

	data.append(trace)

fig = {
	"data": data,
	"layout": {
		"title": "Execution times violin for [%s] -  %s" % (task_types[task_name], task_name),
		"yaxis": {
			"zeroline": False
		}
	}
}

plot(fig, filename="extime-violin-%s-%s.html" % (task_types[task_name], task_name))
pio.write_image(fig, "extime-violin-%s-%s.png" % (task_types[task_name], task_name))
