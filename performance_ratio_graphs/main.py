#!/usr/bin/env python

import os
import argparse
import logging
from plotly.offline import plot
import plotly.graph_objs as go
import plotly.io as pio


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("-l", "--log", help="Set the logging level (Supported values: DEBUG, INFO,\
                        WARNING, ERROR, CRITICAL)", type=str, default="INFO")
    parser.add_argument("-o", "--output", help="Optional output path (Required for when running inside docker)",
                        type=str)
    args = parser.parse_args()

    return args


def set_logging(loglevel):
    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        numeric_level = logging.INFO  # Default level

    logging.basicConfig(level=numeric_level)

def plot_gain_in_execution_time_graph(args):
    x = ["BP", "U-WTol-0%", "U-WTol-25%", "U-WTol-50%", "U-WTol-75%", "U-WTol-100%", "HI-WTol(0/100H)",
         "HI-WTol(25/75H)", "HI-WTol(75/25H)", "HI-WTol(100/0H)", "WlS-WTol", "WlS-WTol-PI-NPI",
         "WlS-WTol-PI-NPI-HeT-LeT"]

    # Data for IPDPS20 graphs. Experiments were rerun by the first consolidating stratos-007 and stratos-008 into one powerclass.
    # So, the cluster was now categorized into 3 powerclasses instead of 4 (as we're only considering CPU power).
    y = [
        -18.08,
        18.31,
        17.47,
        18.58,
        8.49,
        0.61,
        15.76,
        16.71,
        13.18,
        -2.02,
        11.12,
        7.75,
        4.22
    ]

    # bar colors decided based on how energy efficient the experiments were.
    # higher the loss the more towards red the colors are.
    # lower the loss the more towards green the colors are.
    bar_colors = [
            'rgb(127,201,127)', 
            'rgb(190,174,212)', 
            'rgb(253,192,134)', 
            'rgb(166,206,227)', 
            'rgb(31,120,180)', 
            'rgb(178,223,138)', 
            'rgb(51,160,44)', 
            'rgb(251,154,153)', 
            'rgb(227,26,28)',
            'rgb(253,191,111)',
            'rgb(255,127,0)',
            'rgb(202,178,214)',
            'rgb(53,151,143)'
    ]

    # Create a trace
    trace1 = go.Bar(
        x=x,
        y=y,
        marker_color=bar_colors
    )

    layout = {
        'xaxis': {
            'title': ' ',
            'titlefont': {'size': 28, 'color': 'black'},
            'tickfont': {'size': 28, 'color': 'black'},
            'automargin': True,
            'tickangle': -45,

        },
        'yaxis': {
            'title': 'Gain in Execution-Time respect to First-Fit (Seconds)',
            'titlefont': {'size': 27, 'color': 'black'},
            'tickfont': {'size': 30, 'color': 'black'},
            'automargin': True,
            'range':[min(y)-5,max(y)+5]
        },
        'legend': {
            'x': 0.02,
            'y': 1.1,
            'font': {'size': 26, 'color': 'black'},
            'borderwidth': 1
        },
        'height': 1150,
        'width': 1000,
        'margin': go.layout.Margin(t=10, b=0, r=10)
    }

    fig = go.Figure(layout=layout)
    fig.add_trace(trace1)

    output_file_name = 'Gain-ExecTime-vs-First-Fit'
    if args.output:
        output_file_name = os.path.join(args.output, output_file_name)
    else:
        '''
        Output path is not given. So, we're assuming that we're running outside of docker containers.
        (Because output path is needed in case of container, since this script doesn't know where the host
        directory to write output files to, is mounted as volume).
        If we're running outside the container, all the dependencies required by the following "pio"
        functionality (orca and its dependencies like X11 display server) are likely to be installed.
        Hence, this script can write the PNG file directly.
        Otherwise, please open the HTML file written on the volume and save it as PNG.
        ''' 
        pio.write_image(fig, output_file_name+".png")

    plot(fig, filename=output_file_name+'.html')

def plot_loss_in_wait_time_graph(args):
    x = ["BP", "U-WTol-0%", "U-WTol-25%", "U-WTol-50%", "U-WTol-75%", "U-WTol-100%", "HI-WTol(0/100H)",
         "HI-WTol(25/75H)", "HI-WTol(75/25H)", "HI-WTol(100/0H)", "WlS-WTol", "WlS-WTol-PI-NPI",
         "WlS-WTol-PI-NPI-HeT-LeT"]

    # Data for IPDPS20 graphs. Experiments were rerun by the first consolidating stratos-007 and stratos-008 into one powerclass.
    # So, the cluster was now categorized into 3 powerclasses instead of 4 (as we're only considering CPU power).
    y = [
        -188.28,
        3211.03,
        2370.84,
        931.29,
        162.71,
        -59.77,
        2627.57,
        1834.5,
        549.26,
        153.14,
        247.08,
        131.13,
        611.16
    ]

    # bar colors decided based on how energy efficient the experiments were.
    # higher the loss the more towards red the colors are.
    # lower the loss the more towards green the colors are.
    bar_colors = [
            'rgb(127,201,127)', 
            'rgb(190,174,212)', 
            'rgb(253,192,134)', 
            'rgb(166,206,227)', 
            'rgb(31,120,180)', 
            'rgb(178,223,138)', 
            'rgb(51,160,44)', 
            'rgb(251,154,153)', 
            'rgb(227,26,28)',
            'rgb(253,191,111)',
            'rgb(255,127,0)',
            'rgb(202,178,214)',
            'rgb(53,151,143)'
    ]

    # Create a trace
    trace1 = go.Bar(
        x=x,
        y=y,
        marker_color=bar_colors
    )

    layout = {
        'xaxis': {
            'title': ' ',
            'titlefont': {'size': 28, 'color': 'black'},
            'tickfont': {'size': 28, 'color': 'black'},
            'automargin': True,
            'tickangle': -45,

        },
        'yaxis': {
            'title': 'Loss in Wait-Time respect to First-Fit (Seconds)',
            'titlefont': {'size': 27, 'color': 'black'},
            'tickfont': {'size': 30, 'color': 'black'},
            'automargin': True,
            'range':[min(y)-50,max(y)+50]
        },
        'legend': {
            'x': 0.02,
            'y': 1.1,
            'font': {'size': 26, 'color': 'black'},
            'borderwidth': 1
        },
        'height': 1150,
        'width': 1000,
        'margin': go.layout.Margin(t=10, b=0, r=10)
    }

    fig = go.Figure(layout=layout)
    fig.add_trace(trace1)

    output_file_name = 'Loss-WaitTime-vs-First-Fit'
    if args.output:
        output_file_name = os.path.join(args.output, output_file_name)
    else:
        '''
        Output path is not given. So, we're assuming that we're running outside of docker containers.
        (Because output path is needed in case of container, since this script doesn't know where the host
        directory to write output files to, is mounted as volume).
        If we're running outside the container, all the dependencies required by the following "pio"
        functionality (orca and its dependencies like X11 display server) are likely to be installed.
        Hence, this script can write the PNG file directly.
        Otherwise, please open the HTML file written on the volume and save it as PNG.
        ''' 
        pio.write_image(fig, output_file_name+".png")

    plot(fig, filename=output_file_name+'.html')

def plot_loss_in_energy_graph(args):
    x = ["BP", "U-WTol-0%", "U-WTol-25%", "U-WTol-50%", "U-WTol-75%", "U-WTol-100%", "HI-WTol(0/100H)",
         "HI-WTol(25/75H)", "HI-WTol(75/25H)", "HI-WTol(100/0H)", "WlS-WTol", "WlS-WTol-PI-NPI",
         "WlS-WTol-PI-NPI-HeT-LeT"]

    # Data for IPDPS20 graphs. Experiments were rerun by the first consolidating stratos-007 and stratos-008 into one powerclass.
    # So, the cluster was now categorized into 3 powerclasses instead of 4 (as we're only considering CPU power).
    y = [
        141.23,
        1935.82,
        1519.94,
        463.22,
        65.15,
        -22.39,
        1563.66,
        1252.95,
        451.47,
        385.15,
        89.6,
        88.03,
        338.06
    ]

    # bar colors decided based on how energy efficient the experiments were.
    # higher the loss the more towards red the colors are.
    # lower the loss the more towards green the colors are.
    bar_colors = [
            'rgb(127,201,127)', 
            'rgb(190,174,212)', 
            'rgb(253,192,134)', 
            'rgb(166,206,227)', 
            'rgb(31,120,180)', 
            'rgb(178,223,138)', 
            'rgb(51,160,44)', 
            'rgb(251,154,153)', 
            'rgb(227,26,28)',
            'rgb(253,191,111)',
            'rgb(255,127,0)',
            'rgb(202,178,214)',
            'rgb(53,151,143)'
    ]

    # Create a trace
    trace1 = go.Bar(
        x=x,
        y=y,
        marker_color=bar_colors
    )

    layout = {
        'xaxis': {
            'title': ' ',
            'titlefont': {'size': 28, 'color': 'black'},
            'tickfont': {'size': 28, 'color': 'black'},
            'automargin': True,
            'tickangle': -45,

        },
        'yaxis': {
            'title': 'Loss in Energy Consumption with respect to First-Fit (kJ)',
            'titlefont': {'size': 27, 'color': 'black'},
            'tickfont': {'size': 30, 'color': 'black'},
            'automargin': True,
            'range':[-100,2000]
        },
        'legend': {
            'x': 0.02,
            'y': 1.1,
            'font': {'size': 26, 'color': 'black'},
            'borderwidth': 1
        },
        'height': 1150,
        'width': 1000,
        'margin': go.layout.Margin(t=10, b=0, r=10)
    }

    fig = go.Figure(layout=layout)
    fig.add_trace(trace1)

    output_file_name = 'Loss-Energy-vs-First-Fit'
    if args.output:
        output_file_name = os.path.join(args.output, output_file_name)
    else:
        '''
        Output path is not given. So, we're assuming that we're running outside of docker containers.
        (Because output path is needed in case of container, since this script doesn't know where the host
        directory to write output files to, is mounted as volume).
        If we're running outside the container, all the dependencies required by the following "pio"
        functionality (orca and its dependencies like X11 display server) are likely to be installed.
        Hence, this script can write the PNG file directly.
        Otherwise, please open the HTML file written on the volume and save it as PNG.
        ''' 
        pio.write_image(fig, output_file_name+".png")

    plot(fig, filename=output_file_name+'.html')

def plot_gain_in_peak_power_graph(args):
    x = ["BP", "U-WTol-0%", "U-WTol-25%", "U-WTol-50%", "U-WTol-75%", "U-WTol-100%", "HI-WTol(0/100H)",
         "HI-WTol(25/75H)", "HI-WTol(75/25H)", "HI-WTol(100/0H)", "WlS-WTol", "WlS-WTol-PI-NPI",
         "WlS-WTol-PI-NPI-HeT-LeT"]

    # Data for IPDPS20 graphs. Experiments were rerun by the first consolidating stratos-007 and stratos-008 into one powerclass.
    # So, the cluster was now categorized into 3 powerclasses instead of 4 (as we're only considering CPU power).
    y1 = [
        65.55,
        407.16,
        435.74,
        256.31,
        157.38,
        59.44,
        394.93,
        345.81,
        188.66,
        105.24,
        73.78,
        107.35,
        119.43,
    ]

    y2 = [
        43.81,
        436.68,
        405.36,
        278.27,
        118.71,
        21.21,
        356.91,
        341.88,
        153.99,
        106,
        35.34,
        98.98,
        110.13,
    ]

    # Create a trace
    trace1 = go.Bar(
        x=x,
        y=y1,
        name="Gain in Max Peak Power (Watts)",
        marker_color='rgb(255, 127, 14)'
    )

    trace2 = go.Bar(
        x=x,
        y=y2,
        name="Gain in 95th Percentile of Power (Watts)",
        marker_color='rgb(31, 119, 180)'
    )

    data = [trace1, trace2]
    layout = {
        'xaxis': {
            'title': ' ',
            'titlefont': {'size': 28, 'color': 'black'},
            'tickfont': {'size': 28, 'color': 'black'},
            'automargin': True,
            'tickangle': -45,

        },
        'yaxis': {
            'title': 'Gain in Peak Power Consumption (Watts) with respect to First-Fit',
            'titlefont': {'size': 27, 'color': 'black'},
            'tickfont': {'size': 30, 'color': 'black'},
            'automargin': True,
            'range':[0,500]
        },
        'legend': {
            'x': 0.02,
            'y': 1.1,
            'font': {'size': 26, 'color': 'black'},
            'borderwidth': 1
        },
        'height': 1100,
        'width': 1000,
        'margin': go.layout.Margin(t=10, b=0, r=10)
    }

    fig = go.Figure(layout=layout)
    fig.add_trace(trace1)
    fig.add_trace(trace2)

    output_file_name = 'Gain-PeakPower-vs-First-Fit'
    if args.output:
        output_file_name = os.path.join(args.output, output_file_name)
    else:
        '''
        Output path is not given. So, we're assuming that we're running outside of docker containers.
        (Because output path is needed in case of container, since this script doesn't know where the host
        directory to write output files to, is mounted as volume).
        If we're running outside the container, all the dependencies required by the following "pio"
        functionality (orca and its dependencies like X11 display server) are likely to be installed.
        Hence, this script can write the PNG file directly.
        Otherwise, please open the HTML file written on the volume and save it as PNG.
        ''' 
        pio.write_image(fig, output_file_name+".png")

    plot(fig, filename=output_file_name+'.html')

def plot_ratio_percgains_in_power_to_percloss_in_energy_graph(args):
    x = ["BP", "U-WTol-0%", "U-WTol-25%", "U-WTol-50%", "U-WTol-75%", "U-WTol-100%", "HI-WTol(0/100H)",
         "HI-WTol(25/75H)", "HI-WTol(75/25H)", "HI-WTol(100/0H)", "WlS-WTol", "WlS-WTol-PI-NPI",
         "WlS-WTol-PI-NPI-HeT-LeT"]

    # Data for IPDPS20 graphs. Experiments were rerun by the first consolidating stratos-007 and stratos-008 into one powerclass.
    # So, the cluster was now categorized into 3 powerclasses instead of 4 (as we're only considering CPU power).
    y1 = [
        1.183079022,
        0.950096015,
        1.08637006,
        2.435537241,
        7.070781386,
        0.1968870455,
        0.9485384829,
        1.133625505,
        1.387738688,
        1.269438782,
        1.776370816,
        4.463759075,
        #1.210580123
        1.51271273 # rerun-2
    ]

    y2 = [
        1.651048787,
        0.7481941406,
        1.019800344,
        1.968305101,
        8.593088616,
        -5.337091386,
        0.8984459765,
        0.9817885818,
        1.486500648,
        0.9719974079,
        2.929171573,
        4.337959734,
        #1.472298276
        1.256706365 # rerun-2
    ]

    # Data for SC19 graphs. Experiments were conducted with the underlying cluster categorized into 4 powerclasses.
#    y1 = [
#        0.7953120484,
#        0.7991815049,
#        1.197011845,
#        2.52777481,
#        2.217090123,
#        0.1874132712,
#        1.166881723,
#        1.43589126,
#        0.7345363125,
#        0.8605671066,
#        1.566135959,
#        6.728089286,
#        7.490419038
#    ]

#    y2 = [
#        0.5841693754,
#        0.6158565263,
#        0.9301536617,
#        2.440219403,
#        1.780465869,
#        0.003730530888,
#        0.9465087126,
#        1.157648944,
#        0.3977993233,
#        0.1722443324,
#        0.4998146357,
#        4.50485013,
#        4.416128764
#    ]

    # Create a trace
    trace1 = go.Scatter(
        x=x,
        y=y1,
        name="% Gain in 90th Percentile of Power / % Loss in Energy",
        mode='lines+markers',
        line=dict(width=5),
        marker=dict(size=20, symbol=100, line=dict(width=2))
    )

    trace2 = go.Scatter(
        x=x,
        y=y2,
        name="% Gain in Max Peak Power / % Loss in Energy",
        mode='lines+markers',
        line=dict(width=5),
        marker=dict(size=20, symbol=101, line=dict(width=2))
    )

    data = [trace1, trace2]
    layout = {
        'xaxis': {
            'title': ' ',
            'titlefont': {'size': 28, 'color': 'black'},
            'tickfont': {'size': 28, 'color': 'black'},
            'automargin': True,
            'tickangle': -45
        },
        'yaxis': {
            'title': '% Gain in Power / % Loss in Energy with respect to First-Fit',
            'titlefont': {'size': 27, 'color': 'black'},
            'tickfont': {'size': 30, 'color': 'black'},
            'automargin': True,
            'dtick': 1
        },
        'legend': {
            'x': 0.02,
            'y': 1.1,
            'font': {'size': 26, 'color': 'black'},
            'borderwidth': 1
        },
        'height': 1150,
        'width': 1000,
        'margin': go.layout.Margin(t=10, b=0, r=10),
    }
    fig = dict(data=data, layout=layout)

    output_file_name = 'PeakPower-Energy'
    if args.output:
        output_file_name = os.path.join(args.output, output_file_name)
    else:
        '''
        Output path is not given. So, we're assuming that we're running outside of docker containers.
        (Because output path is needed in case of container, since this script doesn't know where the host
        directory to write output files to, is mounted as volume).
        If we're running outside the container, all the dependencies required by the following "pio"
        functionality (orca and its dependencies like X11 display server) are likely to be installed.
        Hence, this script can write the PNG file directly.
        Otherwise, please open the HTML file written on the volume and save it as PNG.
        ''' 
        pio.write_image(fig, output_file_name+".png")

    plot(fig, filename=output_file_name+'.html')


# plot %gains in 90th percentile peak power and max peak power vs % loss in energy.
# this plot should make things easier to interpret instead of plotting the ratio.
def plot_gains_in_power_to_loss_in_energy_graph(args):
    text = ["BP", "U-WTol-0%", "U-WTol-25%", "U-WTol-50%", "U-WTol-75%", "U-WTol-100%", "HI-WTol(0/100H)",
            "HI-WTol(25/75H)", "HI-WTol(75/25H)", "HI-WTol(100/0H)", "WIS-WTol", "WIS-WTol-PI-NPI",
            "WIS-WTol-PI-NPI-HeT-LeT"]

    text = ["<b>"+str(i+1)+". "+text[i]+"</b>" for i in range(len(text))]

    # Data for IPDPS20 graphs. Experiments were rerun by the first consolidating stratos-007 and stratos-008 into one powerclass.
    # So, the cluster was now categorized into 3 powerclasses instead of 4 (as we're only considering CPU power).
    x = [
        4.404387297,
        43.90111492,
        40.75239522,
        27.97555017,
        11.93437151,
        0.4433542109,
        35.88153093,
        34.37050739,
        15.4812052,
        10.65658647,
        3.552865717,
        9.950838955,
        11.07179121
    ]

    y = [
        3.561934739,
        48.82294488,
        38.33411518,
        11.68278276,
        1.643135653,
        -1.103914774,
        39.43676891,
        31.6004116,
        11.38643827,
        9.713794269,
        2.259784413,
        2.220187744,
        8.526146412
    ]

    text_color = ['rgb(214, 39, 40)', 'rgb(31, 119, 180)', 'rgb(31, 119, 180)', 'rgb(31, 119, 180)',
                  'rgb(31, 119, 180)', 'rgb(31, 119, 180)', 'rgb(255, 127, 14)',
                  'rgb(255, 127, 14)', 'rgb(255, 127, 14)', 'rgb(255, 127, 14)', 'rgb(44, 160, 44)',
                  'rgb(44, 160, 44)', 'rgb(44, 160, 44)']

    textposition = ['top left', 'top left', 'top left', 'top left', 'top left', 'bottom left', 'middle left',
                    'top left', 'middle left', 'top center', 'top center', 'top center', 'bottom left']

    n = len(text)
    assert(len(text) == len(x))
    assert(len(x) == len(y))

    data = []
    annotations = []
    # CHANGE THESE VALUES TO REPOSITION MARKERS.
    ax_ay_values = {
        0: {'ax': 25, 'ay': -25},
        1: {'ax': 25, 'ay': -25},
        2: {'ax': 25, 'ay': -25},
        3: {'ax': 25, 'ay': -25},
        4: {'ax': 15, 'ay': -25},
        5: {'ax': 25, 'ay': -25},
        6: {'ax': 25, 'ay': -25},
        7: {'ax': 25, 'ay': -25},
        8: {'ax': 25, 'ay': -25},
        9: {'ax': 25, 'ay': 25},
        10: {'ax': 25, 'ay': -25},
        11: {'ax': -25, 'ay': 25},
        12: {'ax': -25, 'ay': -25},
    }
    for i in range(n):
        trace = {
            'x': [x[i]],
            'y': [y[i]],
            'legendgroup': 'group',
            'name': text[i],
            'mode': 'markers',
            'text': str(i+1),
            'textfont': {
                'size': 25,
                'color': text_color[i],
            },
            'textposition': textposition[i],
            'marker': {
                'size': 20,
                'color': text_color[i],
            }
        }

        annotation = {
            'x': x[i],
            'y': y[i],
            'xref': 'x',
            'yref': 'y',
            'text': str(i+1),
            'showarrow': True,
            'arrowsize': 0.3,
            'arrowhead': 0,
            'arrowwidth': 0.1,
            'ax': ax_ay_values[i]['ax'],
            'ay': ax_ay_values[i]['ay'],
            'align': 'center',
            'font': {
                'size': 30,
                'color': text_color[i]
            }
        }

        data.append(trace)
        annotations.append(annotation)

    layout = {
        'xaxis': {
            'title': '% Gain in 95th Percentile Power with respect to First-Fit',
            'titlefont': {'size': 29, 'color': 'black'},
            'tickfont': {'size': 29, 'color': 'black'},
            'automargin': True
        },
        'yaxis': {
            'title': '% Loss in Energy with respect to First-Fit',
            'titlefont': {'size': 29, 'color': 'black'},
            'tickfont': {'size': 29, 'color': 'black'},
            'automargin': True
        },
        'legend': {
            'x': 0.05,
            'y': 0.95,
            'font': {'size': 25, 'color': 'black'},
            'borderwidth': 1
        },
        'height': 850,
        'width': 950,
        'margin': go.layout.Margin(t=10, r=10, b=10, l=10),
        'annotations': annotations
    }
    fig = dict(data=data, layout=layout)

    output_file_name = 'PercGain95PtilePower-vs-PercLossEnergy'
    if args.output:
        output_file_name = os.path.join(args.output, output_file_name)
    else:
        '''
        Output path is not given. So, we're assuming that we're running outside of docker containers.
        (Because output path is needed in case of container, since this script doesn't know where the host
        directory to write output files to, is mounted as volume).
        If we're running outside the container, all the dependencies required by the following "pio"
        functionality (orca and its dependencies like X11 display server) are likely to be installed.
        Hence, this script can write the PNG file directly.
        Otherwise, please open the HTML file written on the volume and save it as PNG.
        ''' 
        pio.write_image(fig, output_file_name+".png")

    plot(fig, filename=output_file_name+'.html')

def plot_extime_vs_wtime_graph(args):
    text = ["BP", "U-WTol-0%", "U-WTol-25%", "U-WTol-50%", "U-WTol-75%", "U-WTol-100%", "HI-WTol(0/100H)",
            "HI-WTol(25/75H)", "HI-WTol(75/25H)", "HI-WTol(100/0H)", "WIS-WTol", "WIS-WTol-PI-NPI",
            "WIS-WTol-PI-NPI-HeT-LeT"]

    text = ["<b>"+str(i+1)+". "+text[i]+"</b>" for i in range(len(text))]

    # Data for IPDPS20 graphs. Experiments were rerun by the first consolidating stratos-007 and stratos-008 into one powerclass.
    # So, the cluster was now categorized into 3 powerclasses instead of 4 (as we're only considering CPU power).
    x = [
            -16.4573093,
            16.66666667,
            15.90205716,
            16.91243401,
            7.728017477,
            1.957036228,
            14.34553068,
            15.21026761,
            11.9970872,
            -1.838703805,
            10.12197342,
            7.054432915,
            #-1.647551429
            3.841252503 # rerun-2
    ]

    y = [
            -33.80494111,
            576.5279374,
            425.6750934,
            167.2094944,
            29.21394714,
            -13.49109451,
            471.7699655,
            329.3773341,
            98.61749497,
            27.49569089,
            44.36225223,
            23.54388107,
            #56.31643206
            109.731399 # rerun-2
    ]

    # Data for SC19 graphs. Experiments were conducted with the underlying cluster categorized into 4 powerclasses.
#    x = [
#        -9.143104359,
#        11.08112864,
#        11.82026163,
#        14.60227606,
#        10.43365402,
#        4.003637004,
#        13.12327682,
#        12.8959641,
#        4.524989734,
#        - 4.508124597,
#        13.73188831,
#        7.69930193,
#        7.336334839
#    ]
#
#    y = [
#        -46.93851859,
#        526.0006768,
#        444.8540664,
#        163.7267437,
#        57.99588607,
#        - 19.64637768,
#        402.6433321,
#        337.0460328,
#        44.08752666,
#        51.78838098,
#        52.18187215,
#        47.00469718,
#        24.19710824,
#    ]

    text_color = ['rgb(214, 39, 40)', 'rgb(31, 119, 180)', 'rgb(31, 119, 180)', 'rgb(31, 119, 180)',
                  'rgb(31, 119, 180)', 'rgb(31, 119, 180)', 'rgb(255, 127, 14)',
                  'rgb(255, 127, 14)', 'rgb(255, 127, 14)', 'rgb(255, 127, 14)', 'rgb(44, 160, 44)',
                  'rgb(44, 160, 44)', 'rgb(44, 160, 44)']

    textposition = ['top left', 'top left', 'top left', 'top left', 'top left', 'bottom left', 'middle left',
                    'top left', 'middle left', 'top center', 'top center', 'top center', 'bottom left']

    n = len(text)
    assert(len(text) == len(x))
    assert(len(x) == len(y))

    data = []
    annotations = []
    # CHANGE THESE VALUES TO REPOSITION MARKERS.
    ax_ay_values = {
        0: {'ax': 25, 'ay': -25},
        1: {'ax': 25, 'ay': -25},
        2: {'ax': 25, 'ay': -25},
        3: {'ax': 25, 'ay': -25},
        4: {'ax': 15, 'ay': -25},
        5: {'ax': 25, 'ay': -25},
        6: {'ax': 25, 'ay': -25},
        7: {'ax': 25, 'ay': -25},
        8: {'ax': 25, 'ay': -25},
        9: {'ax': 25, 'ay': 25},
        10: {'ax': 25, 'ay': -25},
        11: {'ax': -25, 'ay': 25},
        12: {'ax': -25, 'ay': -25},
    }
    for i in range(n):
        trace = {
            'x': [x[i]],
            'y': [y[i]],
            'legendgroup': 'group',
            'name': text[i],
            'mode': 'markers',
            'text': str(i+1),
            'textfont': {
                'size': 25,
                'color': text_color[i],
            },
            'textposition': textposition[i],
            'marker': {
                'size': 20,
                'color': text_color[i],
            }
        }

        annotation = {
            'x': x[i],
            'y': y[i],
            'xref': 'x',
            'yref': 'y',
            'text': str(i+1),
            'showarrow': True,
            'arrowsize': 0.3,
            'arrowhead': 0,
            'arrowwidth': 0.1,
            'ax': ax_ay_values[i]['ax'],
            'ay': ax_ay_values[i]['ay'],
            'align': 'center',
            'font': {
                'size': 30,
                'color': text_color[i]
            }
        }

        data.append(trace)
        annotations.append(annotation)

    layout = {
        'xaxis': {
            'title': '% Gain in Average Execution-Time with respect to First-Fit',
            'titlefont': {'size': 29, 'color': 'black'},
            'tickfont': {'size': 29, 'color': 'black'},
            'automargin': True
        },
        'yaxis': {
            'title': '% Loss in Average Wait-Time with respect to First-Fit',
            'titlefont': {'size': 29, 'color': 'black'},
            'tickfont': {'size': 29, 'color': 'black'},
            'automargin': True
        },
        'legend': {
            'x': 0.05,
            'y': 0.95,
            'font': {'size': 25, 'color': 'black'},
            'borderwidth': 1
        },
        'height': 850,
        'width': 950,
        'margin': go.layout.Margin(t=10, r=10, b=10, l=10),
        'annotations': annotations
    }
    fig = dict(data=data, layout=layout)

    output_file_name = 'ET-vs-WT'
    if args.output:
        output_file_name = os.path.join(args.output, output_file_name)
    else:
        '''
        Output path is not given. So, we're assuming that we're running outside of docker containers.
        (Because output path is needed in case of container, since this script doesn't know where the host
        directory to write output files to, is mounted as volume).
        If we're running outside the container, all the dependencies required by the following "pio"
        functionality (orca and its dependencies like X11 display server) are likely to be installed.
        Hence, this script can write the PNG file directly.
        Otherwise, please open the HTML file written on the volume and save it as PNG.
        ''' 
        pio.write_image(fig, output_file_name+".png")

    plot(fig, filename=output_file_name+'.html')

def main():
    args = get_args()

    set_logging(args.log)

    # plot_tolerance_breach(args)        # Plot Tolerance breach checker graphs
    plot_ratio_percgains_in_power_to_percloss_in_energy_graph(args) # Plot Gain in Power to Loss in Energy ratio graph
    plot_gains_in_power_to_loss_in_energy_graph(args)
    plot_extime_vs_wtime_graph(args)   # Plot Gain in Execution time to Loss in Wait time graph
    plot_gain_in_peak_power_graph(args)
    plot_loss_in_energy_graph(args)
    plot_loss_in_wait_time_graph(args)
    plot_gain_in_execution_time_graph(args)


if __name__ == '__main__':
    main()
