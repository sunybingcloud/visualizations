# Performance Ratio Graphs

---

```bash
$ python3 main.py -h
usage: main.py [-h] [-l LOG] [-o OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -l LOG, --log LOG     Set the logging level (Supported values: DEBUG, INFO,
                        WARNING, ERROR, CRITICAL)
  -o OUTPUT, --output OUTPUT
                        Optional output path (Required for when running inside
                        docker)
```

---

Format of the command:

```
$ python3 main.py
```

OR

```
$ python3 main.py --output output_dir/
```

---

## Dockerization


### [Docker Hub link](https://cloud.docker.com/repository/docker/vipulchaskar/performance-ratio-graphs)
```
docker pull vipulchaskar/performance-ratio-graphs
```

### Steps to create the docker image


1. cd into this repo and make sure you're having latest changes.
```
cd performance_ratio_graphs
git pull
```


2. Create the docker image and tag it.
```
docker build . -t <TAG_NAME> --no-cache
```


3. Push the created image to your repository, if desired.
```
docker push <TAG_NAME>
```


### Command to run the image

```bash
docker run -v <Local_Path_To_Write_HTML_Files_To>:/tmp/ -it <IMAGE_NAME> --output /tmp/
```

Example 1.

```bash
docker run -v /Users/vipulchaskar/project/visualizations/performance_ratio_graphs/test:/tmp/ -it vipulchaskar/performance-ratio-graphs --output /tmp/
```
