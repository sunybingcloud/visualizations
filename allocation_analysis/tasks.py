all_tasks = {'audio-encoding', 'avrora', 'batik', 'cryptography', 'dgemm',
             'eclipse', 'fop', 'h2', 'jython', 'luindex', 'lusearch',
             'minife', 'network-loopback', 'pmd', 'stream', 'sunflow',
             'tomcat', 'tradebeans', 'video-encoding', 'xalan'}

power_intensive_tasks = {'cryptography', 'video-encoding', 'xalan', 
                              'sunflow', 'dgemm', 'lusearch', 'tomcat'}

high_execution_time_tasks = {'cryptography', 'video-encoding', 'dgemm',
                             'xalan', 'audio-encoding', 'eclipse', 'h2',
                             'tradebeans'}

task_to_num = {}
i = 0
for task in all_tasks:
    task_to_num[i] = task
    i += 1


