#! /usr/bin/python3
import os
import re
import pandas as pd
import numpy as np
import plotly.graph_objs as go
import utils
import tasks
import argparse
from plotly.offline import plot
from sys import exit

# CONSTANTS
DEFAULT_THRESH = 75.32698 # From graphs (value of extime for jython at 100% Tolerance no Alignment)

def setup_args():
    parser = argparse.ArgumentParser(description='Compare exec times at\
                                     different tolerance levels')
    parser.add_argument('-d', '--dirs', nargs='+', help='Directories')
    parser.add_argument('-v', '--val', help='Threshold for a task to have high exec time')
    parser.add_argument('-so', '--sort', nargs='+', help='Sort these tasks')
    parser.add_argument('--tasks', nargs='+',help='Sort these tasks')
    return parser.parse_args()


def get_runtime_data(path):
    runtime = [f for f in utils.all_files_in_dir(path)
               if utils.is_runtime_metrics(f)]
    if len(runtime) != 1:
        print('Didn\'t find correct runtime metrics')
        exit(1)
    runtime = runtime[0]
    df = pd.read_csv(path+runtime, parse_dates=True)
    task_id = df.columns.values[0]
    extime = df.columns.values[-1]
    df = df[[task_id, extime]]
    return df

def get_tolerance(path):
    regex = r'.*-(\d+)perctolerance.*'
    return re.match(regex,path).groups()[0]

def get_path_data(path):
    tasks_vs_extime = {}
    counts = {}
    pat = 'electron-(.*)-\d+-.{8}(:?-.{4}){3}-.{12}'
    df = get_runtime_data(path)
    task_id = df.columns.values[0]
    extime = df.columns.values[-1]
    for index, row in df.iterrows():
        task = re.match(pat, row[task_id]).groups()[0]
        if task not in tasks_vs_extime:
            tasks_vs_extime[task] = 0
            counts[task] = 0
        tasks_vs_extime[task] += row[extime]
        counts[task] += 1
    tasks_avg = []
    tolerance = get_tolerance(path)
    for task in tasks_vs_extime:
        tasks_avg.append([task, tasks_vs_extime[task]/counts[task]])
    tasks_avg = sorted(tasks_avg, key=lambda x: x[0])

    """
    prefix = 'Average execution times at tolerance: ' + str(float(tolerance)/100)
    print('\n')
    print(prefix)
    print('Task name - Average Execution Time')
    print('----------------------------------')
    print('\n')
    for task_data in tasks_avg:
        print(task_data[0] + ' - ' + str(task_data[1]))
    """
    return [tolerance, tasks_avg]

def gen_trace(x, y, name):
    return go.Bar(
        x=x,
        y=y,
        name=name
        )

class Task:
    def __init__(self, name, avg_exec):
        self.name = name
        self.tags = set()
        self.extime = avg_exec

    def add_tag(self, tag):
        self.tags.add(tag)

    def get_extime(self):
        return self.extime

    def get_name(self):
        return self.name

    def __str__(self):
        return self.name
        """
        string = 'Name: ' + self.name + ', Tags: '
        if len(self.tags) == 0:
            string += '{}'
        else:
            string += str(self.tags)
        return string
        """

    def __repr__(self):
        return self.__str__()

pi = "PowerIntensive"
npi = "NonPowerIntensive"
het = "HighExecTime"
nhet = "NonHighExecTime"
def main(args):
    paths = [utils.fixpath(p) for p in args.dirs]
    dfs = []
    for path in paths:
        dfs.append(get_path_data(path))
    if len(dfs) < 1:
        print('No paths')
        exit(1)
    all_tasks = sorted(tasks.all_tasks)
    threshold = args.val if args.val is not None else DEFAULT_THRESH
    data = []
    dfs = sorted(dfs, key=lambda x: int(x[0]))
    task_names = [d[0] for d in dfs[0][1]]
    avgs = [d[1] for d in dfs[0][1]]
    task_objs = []
    for i in range(len(task_names)):
        task_objs.append(Task(task_names[i], avgs[i]))

    cs = {}
    pihet = pi + " & " + het
    pinhet = pi + " & " + nhet
    npihet = npi + " & " + het
    npinhet = npi + " & " + nhet
    cs[pihet] = []
    cs[pinhet] = []
    cs[npihet] = []
    cs[npinhet] = []
    def is_pi(task):
        return utils.is_power_intensive(task)
    tmap = {}
    tsks = sorted(task_objs, key=lambda x: -x.get_extime())
    print(tsks)
    # If median is to be the threshold
    n = len(tsks)
    if n % 2 == 0:
        mex = tsks[n//2].get_extime() + tsks[n//2-1].get_extime()
        threshold = mex/2
    else:
        threshold = tsks[n//2]
    print(threshold) 
    for i in range(len(task_objs)):
        name = task_objs[i].get_name()
        tmap[name] = task_objs[i]
        extime = task_objs[i].get_extime()
        if is_pi(name) and extime > threshold:
            cs[pihet].append(task_objs[i])
        elif is_pi(name) and extime <= threshold:
            cs[pinhet].append(task_objs[i])
        elif not is_pi(name) and extime > threshold:
            cs[npihet].append(task_objs[i])
        else:
            cs[npinhet].append(task_objs[i])
    for c in cs.keys():
        cs[c] = sorted(cs[c], key=lambda x: -x.get_extime())
    for c in cs.keys():
        print(c + ": " + str(cs[c]))

    pinhetnpinhet = []
    for t in cs[pinhet]:
        pinhetnpinhet.append(t)
    for t in cs[npihet]:
        pinhetnpinhet.append(t)
    pinhetnpinhet = sorted(pinhetnpinhet, key=lambda x: -x.get_extime())
    print(pinhetnpinhet)
    if args.tasks is not None:
        names = set(args.tasks)
        tos = [tsk_obj for tsk_obj in tsks if tsk_obj.get_name() in names]
        tos = sorted(tos, key=lambda x: -x.get_extime())
        print('-------------')
        
        print('-------------')
        print('-------------')
        print(tos)
    if args.sort is None:
        return
    st = sorted(tasks.all_tasks, key=lambda x: -tmap[x].get_extime())
    
    

if __name__ == '__main__':
    args = setup_args()
    main(args)
        
