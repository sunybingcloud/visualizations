#! /usr/bin/python3
import pandas as pd
import numpy as np
import re
import plotly.graph_objs as go
import argparse
import os
import utils
import tasks
import sections
from plotly.offline import plot
from sys import exit

# CONSTANTS
section_regex = r'.*electron-(.*)-\d+-.{8}(?:-.{4}){3}-.{12}-(.*)'
font_size = 24
def setup_args():
    parser = argparse.ArgumentParser(
                description='Plot execution times of tasks across different sections \
                        \n ONLY RUN WITH EXPERIMENTAL TASK ID WITH SECTION NAME IN IT')
    parser.add_argument('-d', '--dir', help='Path to electron log directory', required=True)
    parser.add_argument('--out', help='Output directory path to save files', required=True)
    parser.add_argument('-s', '--save', help='html filename to save file as', required=True)
    return parser.parse_args()

def parse_runtime_file(path):
    section_wise = {}
    with open(path) as f:
        f.readline()
        for line in f:
            task_id, extime_str = line.split(",")[0], line.split(",")[-1]
            match = re.match(section_regex, task_id)
            if match is None:
                print("Doesn't have section name, exiting...")
                exit(1)
            task_name = match.groups()[0]
            section = match.groups()[1]
            extime = float(extime_str)
            if section not in section_wise:
                section_wise[section] = {}
            if task_name not in section_wise[section]:
                section_wise[section][task_name] = []
            section_wise[section][task_name].append(extime)
    result = {}
    for section in section_wise:
        count = {}
        total = {}
        for task in section_wise[section]:
            total[task] = sum(section_wise[section][task])
            count[task] = len(section_wise[section][task])
        for task in total:
            if section not in result:
                result[section] = []
            result[section].append([task, total[task]/count[task]])
        result[section] = sorted(result[section], key=lambda x: x[0])
    return result

def gen_trace(x, y, name):
    return go.Bar(
            x=x,
            y=y,
            name=name)

def display_or_save(fig, out_dir, save):
    if not save:
        plot(fig, filename=out_dir + 'comparePower.html')
    else:
        plot(fig, filename=out_dir+save+'_comparePower.html', image='png', image_filename=save)

def main(args):
    output_dir = utils.fixpath(args.out)
    if not output_dir:
        print("Invalid output directory")
        print("Exiting...")
        exit(1)
    path = utils.fixpath(args.dir)
    all_files = utils.all_files_in_dir(path)
    runtimeFile = [f for f in all_files if utils.is_runtime_metrics(f)][0]
    section_wise = parse_runtime_file(path + runtimeFile)
    data = []
    all_tasks = sorted(tasks.all_tasks)
    for section in section_wise:
        avgs = [avg[1] for avg in section_wise[section]]
        trace = gen_trace(all_tasks, avgs, name=section)
        data.append(trace)
        #print(section_wise[section])
        #print("")
    layout = go.Layout(height=1050,
                       width=1680,
                       barmode='group',
                       legend=dict(orientation='h', x=0.3, y=1.05,
                                   font=dict(size=font_size)),
                       titlefont=dict(size=font_size),
                       xaxis=dict(title='Tasks', tickfont=dict(size=font_size)),
                       yaxis=dict(title='Execution Time (s)', tickfont=dict(size=font_size)))
    fig = go.Figure(data=data, layout=layout)
    display_or_save(fig, output_dir, args.save)

if __name__ == '__main__':
    args = setup_args()
    main(args)
