#! /usr/bin/python3
import utils
import analysis
import argparse
import re
import plotly.graph_objs as go
import os
import motivation_single as bs
from plotly.offline import plot
from sys import exit

def setup_args():
    parser = argparse.ArgumentParser('Compare power consumptions')
    parser.add_argument('-d', '--dir', nargs='+', help='Directories', required=True)
    parser.add_argument('-o', '--out', help='Output directory', required=True)
    parser.add_argument('-s', '--save', help='Filename to save html as', required=True)
    return parser.parse_args()

def main(args):
    out_dir = utils.fixpath(args.out)
    if not utils.is_valid_path(out_dir):
        print("Invalid output directory")
        print("Exiting...")
        exit(1)
    dfs = []
    for d in args.dir:
        pcp_df = bs.get_pcp_data(d, 'stratos-005')
        dfs.append([d, pcp_df])
    data = []
    dash = True
    for pdf in dfs:
        name = pdf[0].split('Electron-')[1].split('-Motivation')[0]
        if dash:
            data.append(go.Scatter(
                y=pdf[1],
                x=list(range(len(pdf[1]))),
                mode='lines',
                name=name,
                line=dict(dash="dash")))
        else:
            data.append(go.Scatter(
                y=pdf[1],
                x=list(range(len(pdf[1]))),
                mode='lines',
                name=name))
        dash = not dash
    layout = go.Layout(xaxis=dict(title='Time (seconds)',
                                  titlefont=dict(size=26),
                                  tickfont=dict(size=22)),
                       yaxis=dict(title='Power (Watts)',
                                  titlefont=dict(size=26),
                                  tickfont=dict(size=22)),
                       legend=dict(x=0.6,
                                   y=1.3,
                                   borderwidth=2,
                                   bgcolor='#E2E2E2',
                                   font=dict(size=22)))
    fig = go.Figure(data=data, layout=layout)
    plot(fig, filename=out_dir+args.save+'_comparePower.html')

if __name__ == '__main__':
    args = setup_args()
    main(args)
