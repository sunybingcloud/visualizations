#! /usr/bin/python3
import pandas as pd
import numpy as np
import re
import plotly.graph_objs as go
import argparse
import os
from plotly import tools
from plotly.offline import plot
from sys import exit
import utils
import policy

# CONSTANTS
pi = 'power_intensive'
npi = 'non_power_intensive'
het = 'high_execution_time_task'
nhet = 'non_high_execution_time_task'
hosts = utils.get_all_hosts()
sections = 10
# Based on values mentioned in powerSpecs.json @xavier
default_total_tdp = 1460
# Based on looking at graph for 100% tolerance
total_max_tasks = 160
font_size = 50
tick_font_size = 42
axes_color = '#000000' #Black
height = 800
width = 1000

def setup_args():
    parser = argparse.ArgumentParser(
                description='Plot power consumption at each host')
    parser.add_argument('-d', '--dir',
                        help='Path to electron log directory', required=True)
    parser.add_argument('--out', help='Output directory to save files', required=True)
    parser.add_argument('-s', '--save', help='Name of file to save html as', required=True)
    parser.add_argument('-pp', help='Add if power profile is also to be shown')
    parser.add_argument('-t', '--total', help='Total clusterwide tdp\
                        available, default = ' + str(default_total_tdp))
    parser.add_argument('-tfs', help='Tick font size (Optional)')
    return parser.parse_args()

def parse_data(line):
    regex = r'\S+\s(\S+)\s(.*)$'
    match = re.match(regex, line)
    line = match.groups()[1]
    data_per_host = line.split('|')
    power_intensive_tasks = 0
    het_tasks = 0
    nhet_tasks = 0
    non_power_intensive_tasks = 0
    for host_data in data_per_host:
        host = host_data.split(':')[0]
        tasks = host_data.split(':')[1:-1]
        for task in tasks:
            task_id = task.split('electron-')[1]
            if utils.is_power_intensive(task_id, True):
                power_intensive_tasks += 1
            else:
                non_power_intensive_tasks += 1
            if utils.is_het_task(task_id, True):
                het_tasks += 1
            else:
                nhet_tasks += 1

    return {pi: [power_intensive_tasks],
             npi: [non_power_intensive_tasks],
             het: [het_tasks],
             nhet: [nhet_tasks]} 


def get_allocation_metrics_data(path):
    path = utils.fixpath(path)
    all_files = utils.all_files_in_dir(path)
    alloc_metrics = None
    for fname in all_files:
        if utils.is_allocation_metrics(fname):
            alloc_metrics = fname
            break
    if alloc_metrics is None:
        print('Did not find allocation Metrics file')
        print('Exiting...')
        exit(1)
    dfs = []
    with open(path+alloc_metrics) as f:
        f.readline()
        for line in f:
            line_dict = parse_data(line)
            line_df = pd.DataFrame.from_dict(line_dict)
            dfs.append(line_df)
    return pd.concat(dfs, ignore_index=True)

def get_policy(dirname):
    for p in policy.policies:
        if p in dirname:
            return p
    return 'WaaR'

def get_pcp_data(path):
    path = utils.fixpath(path)
    all_files = utils.all_files_in_dir(path)
    pcp_file = None
    for fname in all_files:
        if utils.is_pcp(fname):
            pcp_file = fname
            break
    if pcp_file is None:
        print('Did not find pcplog file')
        print('Exiting...')
        exit(1)
    pcp_df = pd.read_csv(path+pcp_file, parse_dates=True)
    pcp_df = utils.filter_and_normalize_cpu_data(pcp_df)
    pcp_df = utils.gen_clusterwide_total_power(pcp_df)
    return pcp_df

def display_or_save(fig, out_dir, save):
    if not save:
        plot(fig, filename=out_dir+'taskDistribution.html')
    else:
        plot(fig, filename=out_dir+save+'_taskDistribution.html', image='png', image_filename=save)

def get_tolerance(dirname):
   regex = r'.*-(\d+)perctolerance.*'
   return int(re.match(regex, dirname).groups()[0]) if re.match(regex, dirname) else 0

def gen_scatter(xlist, ylist, mode, name, fill = None):
    return go.Scatter(
        x=xlist,
        y=ylist,
        mode=mode,
        name=name,
        fill=fill
        )
"""
    Prints out range where PI dominates or NPI dominates
"""
def generate_stats(df, pdf):
    class DominatingRange:
        def __init__(self, task_type, task_range, avg_pwr, tasks, peak):
            self.type = task_type
            self.range = task_range
            self.avg_pwr = avg_pwr
            self.tasks = tasks
            self.peak_pwr = peak

        def __str__(self):
            string = ""
            if self.type == pi:
                string = 'Power Intensive task dominating: '
            else:
                string += 'Non Power Intensive task dominating: '
            string += str(self.range)
            string += ', avg dominating tasks: ' + str(self.tasks)
            string += ', avg power: ' + str(self.avg_pwr) + ' Watts'
            string += ', peak power: ' + str(self.peak_pwr) + ' Watts'
            return string
    i = 0
    time = len(df)-1
    start = 0
    end = 0
    tt = pi
    ranges = []
    while i < time:
        pwr = 0
        peak = -1
        tasks = 0
        if df[pi][i] > df[npi][i]:
            start = i
            tt = pi
            while i < time and df[pi][i] > df[npi][i]:
                tasks += df[pi][i]
                peak = max(peak, pdf[i])
                pwr += pdf[i]
                i += 1
            end = i
        else:
            start = i
            tt = npi
            while i < time and df[npi][i] >= df[pi][i]:
                tasks += df[npi][i]
                peak = max(peak, pdf[i])
                pwr += pdf[i]
                i += 1
            end = i
        ranges.append(DominatingRange(tt,
                                      [start, end],
                                      pwr/(end-start),
                                      tasks/(end-start),
                                      peak))
    for dr in ranges:
        print(dr)

def main(args):
    global tick_font_size
    if not utils.is_valid_path(args.out):
        print("Invald output directory")
        print("Exiting...")
        exit(1)
    total_max = args.total if args.total is not None else default_total_tdp
    power_yscale = (0, total_max)
    tasks_yscale = (0, total_max_tasks) 
    df = get_allocation_metrics_data(args.dir)
    policy = get_policy(args.dir)
    tolerance = get_tolerance(args.dir) if policy is 'WaaR' else 0
    profile = 'medmedmed' if 'medmedmed' in args.dir else 'medmedmax'
    pcp_df = get_pcp_data(args.dir)
    time = list(range(len(df)))
    mode = 'lines'
    #generate_stats(df, pcp_df)
    tick_font_size = float(args.tfs) if args.tfs is not None else tick_font_size
    font_size = float(args.tfs) if args.tfs is not None else tick_font_size
    pi_trace = gen_scatter(time, df[pi], mode, 'PI Tasks')
    npi_trace = gen_scatter(time, df[npi], mode, 'NPI Tasks')
    het_trace = gen_scatter(time, df[het], mode, 'HeT Tasks')
    nhet_trace = gen_scatter(time, df[nhet], mode, 'LeT Tasks')
    layout2 = go.Layout(legend=dict(orientation='h',x=0.5, y=1.25, font=dict(size=font_size), borderwidth=1),
                        xaxis=dict(title='Time (seconds)', titlefont=dict(size=font_size),
                                   tickfont=dict(size=tick_font_size), zerolinecolor=axes_color),
                        yaxis=dict(title='# of Tasks', titlefont=dict(size=font_size),
                                   tickfont=dict(size=tick_font_size),
                                   range=tasks_yscale, zerolinecolor=axes_color),
                        height=height, width=width,
                        margin=go.layout.Margin(
                            l=130, b=110
                            ))
    fig = go.Figure([pi_trace, npi_trace, het_trace, nhet_trace], layout=layout2)
    if not args.pp:
        display_or_save(fig, args.out, args.save)
        return
    power_trace = gen_scatter(time, pcp_df, mode, 'Actual Consumed power',
                              'tozeroy')
    max_power_trace = gen_scatter(time, [total_max for i in time], mode,
                                  'Total Max power', 'tonexty')
    xaxis1 = 'xaxis1'
    xaxis2 = 'xaxis2'
    yaxis1 = 'yaxis1'
    yaxis2 = 'yaxis2'
    layout = 'layout'
    figt = tools.make_subplots(rows=2, cols=1,
                               subplot_titles=('Number of Tasks vs Time',
                                               'Power vs Time'))
    figt.append_trace(pi_trace, 1, 1)
    figt.append_trace(npi_trace, 1, 1)
    figt.append_trace(power_trace, 2, 1)
    figt.append_trace(max_power_trace, 2, 1)
    figt[layout][xaxis1].update(title='Time (in seconds)', titlefont=dict(size=font_size), tickfont=dict(size=font_size))
    figt[layout][xaxis2].update(title='Time (in seconds)', titlefont=dict(size=font_size), tickfont=dict(size=font_size))
    figt[layout][yaxis1].update(title='# of Tasks', titlefont=dict(size=font_size), tickfont=dict(size=font_size), range=tasks_yscale)
    figt[layout][yaxis2].update(title='Power (in Watts)', titlefont=dict(size=font_size), tickfont=dict(size=font_size), range=power_yscale)
    figt[layout].update(legend=dict(orientation='h', x=0.2, y=0.5, font=dict(size=font_size)))
    figt[layout].update(height=1050, width=1680)
    for i in figt[layout]['annotations']:
        i['font'] = dict(size=font_size)
    display_or_save(figt, args.out, args.save)

if __name__ == '__main__':
    args = setup_args()
    main(args)
