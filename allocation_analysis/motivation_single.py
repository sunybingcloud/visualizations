#! /usr/bin/python3
import pandas as pd
import utils
import analysis
import argparse
import re
import plotly.graph_objs as go
import os
from plotly.offline import plot
from sys import exit

def setup_args():
    parser = argparse.ArgumentParser('Compare power consumptions')
    parser.add_argument('-d', '--dir', help='Electron log directory', required=True)
    parser.add_argument('--idle', help='idle power', required=True)
    parser.add_argument('--out', help='Output directory path', required=True)
    parser.add_argument('-s', '--save', help='Filename to save html as', required=True)
    return parser.parse_args()

def get_pcp_data(path, host):
    path = utils.fixpath(path)
    pcp_file = [f for f in utils.all_files_in_dir(path)
                if utils.is_pcp(f)][0]
    pcp_df = pd.read_csv(path+pcp_file, parse_dates=True)
    pcp_df = utils.filter_and_normalize_cpu_data(pcp_df)
    regex = host
    pcp_df = pcp_df.filter(regex=regex)
    pcp_df['total'] = pcp_df.sum(axis=1)
    return pcp_df['total']

def main(args):
    out_dir = utils.fixpath(args.out)
    if not utils.is_valid_path(out_dir):
        print("Invalid output directory")
        print("Exiting...")
        exit(1)
    pcp_df = get_pcp_data(args.dir, 'stratos-007')
    name = args.dir.split('Electron-')[1].split('-Motivation')[0]
    dfs = []
    dfs.append([name, pcp_df])
    static_pwr = args.idle
    data = []
    title = ''
    for pdf in dfs:
        data.append(go.Scatter(
            y=pdf[1],
            x=list(range(len(pdf[1]))),
            mode='lines',
            name=pdf[0]
            ))
        title=pdf[0]
    idle_df = [static_pwr for i in range(len(dfs[0][1]))]
    text = 'Median static power'
    text_d = ['' for i in range(len(idle_df)//2)]
    text_d.append(text)
    data.append(go.Scatter(
        y=idle_df,
        x=list(range(len(idle_df))),
        mode='lines+text',
        line=dict(dash='dash'),
        text=text_d,
        textposition='top center',
        textfont=dict(size=26),
        name=text
        ))
    titlefont=30
    tickfont=22
    layout = go.Layout(xaxis=dict(title='Time (seconds)',
                                  titlefont=dict(size=titlefont),
                                  tickfont=dict(size=tickfont)),
                       yaxis=dict(title='Power (W)',
                                  titlefont=dict(size=titlefont),
                                  tickfont=dict(size=tickfont)),
                       showlegend=False,
                       legend=dict(x=0.5,
                                   y=0.50,
                                   borderwidth=2,
                                   bgcolor='#E2E2E2',
                                   font=dict(size=22)))
    fig = go.Figure(data=data, layout=layout)
    plot(fig, filename=out_dir+args.save+'_powerCompare.html')

if __name__ == '__main__':
    args = setup_args()
    main(args)
