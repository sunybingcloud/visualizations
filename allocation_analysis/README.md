# Allocation analysis
----------
Plots the distribution of PI, NPI, HeT, LeT tasks per second on the cluster against execution time.
Also plots power profile against execution time.

python3 analysis.py -d <log-directory> --out <\output-directory> [-s <savefile>, -t <total-max-power[default=1460]>]

Example:
> python3 analysis.py -d /home/user/Desktop/temp/log-dir/ --out /home/user/Desktop/temp/out-dir/ 

To plot power profile against execution time:
> python3 analysis.py -d /home/user/Desktop/temp/log-dir/ --out /home/user/Desktop/temp/out-dir/ -pp <any-value>

With image filename and TDP (total-max-power)
> python3 analysis.py -d /home/user/Desktop/temp/log-dir/ --out /home/user/Desktop/temp/out-dir/ -s filename -t 1500

# Motivation (Combined)
----------
Plots the power profile for multiple tasks when ran simultaneously on a given host along with the power profile when
the tasks are ran independently against execution time.

Instructions to run motivaiton.py

python3 motivation.py -isp <idle-static-power> -id [<directory1>, <directory2> ...] -d <log-directory-for-combined-run> --out <\output-directory>

Example:
> python3 motivation.py -isp 15.8720 -id /home/user/Desktop/dir1 /home/user/Desktop/dir2 -d /home/user/Desktop/dir3-combined --out /home/user/Desktop/out-dir

# Sections vs Extime
----------
Plots the execution time for each task in different sections. Sections are:
* Power Intensive
* Non Power Intensive
* Sparse
* Dense

Can only be used with runs in which the tasks are tagged with the section names they were launched in.

Instructions to run section\_vs\_extime.py

python3 section\_vs\_extime.py -d <log-directory> [-s <savefile>] --out <\output-dir>

Example:
> python3 section\_vs\_extime.py -d /home/user/Desktop/log-dir --out /home/user/Desktop/out-dir -s filename

# Motivation (Single)
----------
Plots the power profile for a single task ran on a single host while showing the idle static power consumption for that host as well.

Instructions to run motivation\_single.py

python3 motivation\_single.py -d <log-directory> --out <\output-directory> --name <task-name> [--idle <idle-power>]

Example:
> python3 motivation\_single.py -d /home/user/Desktop/log-dir --out /home/user/Desktop/out-dir --idle 15.8720

# Compare Power
------------
Plots power profile for multiple runs together and compares them against time

Instructions to run compare\_power.py

python3 compare\_power.py -d [<dir1>, <dir2>...] --out <out-directory> --save <filename>

Example:
> python3 compare\_power.py -d /data/Electron-WaaR-run1 /data/Electron-WaaR-run2 --out /output-dir --save testfile

## Setting up the Docker container
------------

Build and tag the Docker image using the given Dockerfile, command:
> docker build --tag=<tag-name> -f <path>/Dockerfile .


Run all scripts in the container with the tag-name as follows:
> docker run -v <path-to-\output-\dir>:/output -v <path-to-logs>:/data -v <path-to-scripts>:/scripts <tag-name> /scripts/<\script-name> <arg1> <arg2> <arg3>

Example for all scripts:


Allocation Analysis

> docker run -v /user/Desktop/output:/output -v /user/data/:/data -v /user/scripts/:/scripts sc19-image /scripts/analysis.py -d /data/Electron-WaaR-Run1 --out /output/ --save fileName


Motivation (combined)

> docker run -v /user/Desktop/output:/output -v /user/data/:/data -v /user/scripts/:/scripts sc19-image /scripts/motivation.py -isp 15.8720 -id /data/Electron-WaaR-dir1 /data/Electron-WaaR-dir2 -d /data/Electron-WaaR-dir3-combined --out /output --save fileName


Section vs Extime

> docker run -v /user/Desktop/output:/output -v /user/data/:/data -v /user/scripts/:/scripts sc19-image /scripts/section\_vs\_extime.py -d /data/Electron-WaaR-run1 --out /output -s filename


Motivation (Single)

> docker run -v /user/Desktop/output:/output -v /user/data/:/data -v /user/scripts/:/scripts sc19-image /scripts/motivation\_single.py -d /data/Electron-WaaR-Run1/ --out /output --save fileName --idle 15.8720


Compare Power

> docker run -v /user/Desktop/output:/output -v /user/data/:/data -v /user/scripts/:/scripts sc19-image /scripts/compare\_power.py -d /data/Electron-WaaR-run1 /data/Electron-WaaR-run2 --out /output --save testFile 
