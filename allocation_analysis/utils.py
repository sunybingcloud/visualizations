import os
import re
import pandas as pd
import numpy as np
from sys import exit
import tasks

"""
  All utilities that does not logically
  belong in the script added here
"""
# CONSTANTS
pcplog = '.pcplog'
sched_trace = '_schedTrace.log'
allocation_stats = '_allocationStats.log'
allocation_metrics = '_allocationMetrics.log'
workload_sections = '_workloadSections.log'
console = '_console.log'
runtime = '_runtimeMetrics.log'

def is_valid_path(path):
    return os.path.isdir(path)

def is_runtime_metrics(f):
    return f.endswith(runtime)

def file_exists(f):
    return os.path.exists(f)

def is_console(fname):
    return fname.endswith(console)

def is_workload_section(fname):
    return fname.endswith(workload_sections)

def is_sched_trace(fname):
    return fname.endswith(sched_trace)

def is_pcp(fname):
    return fname.endswith(pcplog)

def is_allocation_stats(fname):
    return fname.endswith(allocation_stats)

def is_allocation_metrics(fname):
    return fname.endswith(allocation_metrics)

def fixpath(path):
    return path if path.endswith('/') else path+'/'

def all_files_in_dir(directory):
    return os.listdir(directory)

"""
  Read the schedTrace log file and extract
  all the information into seperate columns
  columns = ['date', 'time', 'host', 'task', 'task_id'] 
"""
def extract_sched_trace_data(f):
    if not file_exists(f):
        print('File does not exist: ' + f)
        print('Exiting...')
        return None
    columns = ['date', 'time', 'host', 'task', 'task_id']
    df = pd.read_csv(f, names=['data'], parse_dates = True)
    # (Date) (Time) (Host).cs.binghamton...:electron-(taskname)-instance-(Task Id)
    regex = r'(\S+)\s(\S+)\s(stratos-.*).cs.binghamton.edu:electron-(.*)-\d+-(.{8}-.{4}-.{4}-.{4}-.{12})' 
    df = pd.DataFrame(np.array(df['data'].str.extract(regex)), columns=columns)
    return df

def is_power_intensive(task, is_task_id=False):
    if not is_task_id:
        return task in tasks.power_intensive_tasks
    regex = '(.*)-\d+-.{8}(:?-.{4}){3}-.{12}'
    taskname = re.match(regex, task).groups()[0]
    return taskname in tasks.power_intensive_tasks

def is_het_task(task, is_task_id=False):
    if not is_task_id:
        return task in tasks.high_execution_time_tasks
    regex = '(.*)-\d+-.{8}(:?-.{4}){3}-.{12}'
    taskname = re.match(regex, task).groups()[0]
    return taskname in tasks.high_execution_time_tasks

def get_all_hosts():
    hosts = []
    prefix = 'stratos-00'
    for i in range(1, 9):
        hosts.append(prefix+str(i))
    return hosts

# Filters out unused columns and converts power to Watts
# power_in_watts = power/2^32
# returns - filtered_normalized_pcp_df
def filter_and_normalize_cpu_data(pcp_dataframe):
    filter_term = ':perfevent.hwcounters.rapl__RAPL_ENERGY_PKG'
    unit = 2**32
    pcp_dataframe = pcp_dataframe.filter(regex=filter_term)
    pcp_dataframe = pcp_dataframe.divide(unit)
    return pcp_dataframe

# returns only the total cluster wide power column calculated
def gen_clusterwide_total_power(pcp_dataframe):
    pcp_dataframe['total-power'] = pcp_dataframe.sum(axis=1)
    return pcp_dataframe['total-power']

