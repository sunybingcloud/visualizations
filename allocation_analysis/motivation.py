
#! /usr/bin/python3
import utils
import analysis
import argparse
import motivation_single as ms
import re
import plotly.graph_objs as go
import os
import numpy as np
from plotly.offline import plot
from sys import exit

def setup_args():
    parser = argparse.ArgumentParser('Compare power consumptions')
    parser.add_argument('--out', help='Output directory path', required=True)
    parser.add_argument('-isp', '--idle', help='Idle static power', required=True)
    parser.add_argument('-id', '--indir', nargs='+', help='Individual runs', required=True)
    parser.add_argument('-d', '--dir', help='Combined directory', required=True)
    parser.add_argument('-s', '--save', help='Filename to save html as', required=True)
    return parser.parse_args()

def main(args):
    out_dir = utils.fixpath(args.out)
    if not utils.is_valid_path(out_dir):
        print("Invalid output directory")
        print("Exiting...")
        exit(1)
    individual_vals = []
    for path in args.indir:
        pwr_arr = ms.get_pcp_data(path, 'stratos-007')
        individual_vals.append(pwr_arr)
    indi_vals = sorted(individual_vals, key=lambda x: -len(x))
    added_vals = indi_vals[0]
    idle_pwr = float(args.idle)
    for i in range(1, len(indi_vals)):
        for j in range(len(indi_vals[i])):
            added_vals[j] += indi_vals[i][j]
    without_rem = list(added_vals)
    for i in range(len(indi_vals)-1):
        for j in range(len(indi_vals[1])):
            added_vals[j] -= idle_pwr
    combined_vals = ms.get_pcp_data(args.dir, 'stratos-007')
    xaxis = np.array(list(range(len(combined_vals))))
    xaxis2 = np.array(list(range(len(added_vals))))
    added_scatter = go.Scatter(x=list(range(len(combined_vals))),
                               y=added_vals,
                               name='Added power',
                               line=dict(dash='dash'))
    combined_scatter = go.Scatter(x=list(range(len(combined_vals))),
                                  y=combined_vals,
                                  name='Coscheduled Power')
    layout = go.Layout(xaxis=dict(title='Time (seconds)',
                                  titlefont=dict(size=26),
                                  tickfont=dict(size=22)),
                       yaxis=dict(title='Power (W)',
                                  titlefont=dict(size=26),
                                  tickfont=dict(size=22)),
                       legend=dict(x=1.1,
                                   y=1.2,
                                   borderwidth=2,
                                   bgcolor='#E2E2E2',
                                   font=dict(size=22)))
    fig = go.Figure(data=[added_scatter,combined_scatter], layout=layout)
    plot(fig, filename=out_dir+args.save+'_combinedPowerCompare.html')

if __name__ == '__main__':
    args = setup_args()
    main(args)

